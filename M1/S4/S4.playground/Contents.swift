////: Playground - noun: a place where people can play
//
import Foundation
//P1
var city: String = "Detroit"
var color: String = "Red"
var object: String = "Wings"
let concatenedString = city + color + object
let interpolatedString = "\(city)\(color)\(object)"
print("\(concatenedString)")
print("\(interpolatedString)")
//P2
func diff(parentString: String, subString: String) -> String {
    var newString = ""
    if parentString.range(of: subString) == nil {
        print("\(subString) not found in \(parentString)")
    } else {
        newString = parentString.replacingOccurrences(of: subString, with: "")
        print("new string = \(newString)")
    }
    return newString
}
var a = "abc"
var b = "c"
diff(parentString: a, subString: b)
//P3
func removeChar(theString: String, theChar: Character){
    var new: String = ""
    for char in theString.characters {
        if char == theChar {
            new.append("")
        } else {
            new.append(char)
        }
    }
    print("\(new)")
}
removeChar(theString: "penta stagiu swift", theChar: "a")
//P4

func countWords(sentence: String, word: String) {
    let helper = sentence.components(separatedBy: word)
    print(helper.count - 1)
}
countWords(sentence: "aceasta este este este o prop", word: "este")

//P5

func numberInArray(n: Double, arrayOfNumbers: [Double]){
    var isInArray: Int = 0
    for index in 0 ..< arrayOfNumbers.count {
        if n == arrayOfNumbers[index] {
           isInArray = isInArray + 1
        }
    }
    if isInArray > 0{
        print("\(n) is in the array \(arrayOfNumbers)")
    } else {
        print("\(n) is not in the array \(arrayOfNumbers)")
    }
}
numberInArray(n: 2, arrayOfNumbers: [1, 3, 4, 2, 22, 43, 1, 43, 24])
numberInArray(n: 2, arrayOfNumbers: [1.23, 3, 2.32, -1.23])
//P6
func deleteFromArray(n: Double, arrayOfDouble: inout [Double] ) {
    if arrayOfDouble.contains(n){
    arrayOfDouble = arrayOfDouble.filter(){$0 != n}
        print("\(arrayOfDouble)")
    } else {
     print("\(n) not in array \(arrayOfDouble)")
    }
}
var arrayDoubles: [Double] = [4, 2, 6, 9, 4, 4.5]
deleteFromArray(n: 5, arrayOfDouble: &arrayDoubles)
deleteFromArray(n: 4, arrayOfDouble: &arrayDoubles)

// P7
let myDictionary = [ "math": ["plus" , "minus", "sum"],
                     "vocals": ["a", "e", "i", "o", "u"],
                     "actions" : ["run", "stay" , "play"],
]
for aKey in myDictionary.keys {
    if let myArray = myDictionary[aKey] as? [String] {
        print(myArray.sorted())
    }
}
//P8
enum HandShape {
    case rock
    case paper
    case scissor
}

enum GameState {
    case win
    case lose
    case draw
}

func rockPaperScrissor(firstPlayer: HandShape,secondPlayer: HandShape) -> GameState {
    var state: GameState
    switch firstPlayer {
    case .paper:
        switch secondPlayer {
        case .paper:
            state = .draw
            print("Draw")
        case .rock:
            state = .win
            print("You win")
        case .scissor:
            state = .lose
            print("You lose loser")
        }
    case .scissor:
        switch secondPlayer {
        case .scissor:
            state = .draw
            print("Draw")
        case .paper:
            state = .win
            print("You win")
        case .rock:
            state = .lose
            print("You lose loser")
        }
    case .rock:
        switch secondPlayer {
        case .rock:
            state = .draw
            print("Draw")
        case .paper:
            state = .lose
            print("You lose loser")
        case .scissor:
            state = .win
            print("You win")
        }
    }
 return state
}
rockPaperScrissor(firstPlayer: .paper, secondPlayer: .rock)
rockPaperScrissor(firstPlayer: .paper, secondPlayer: .scissor)
rockPaperScrissor(firstPlayer: .paper, secondPlayer: .paper)
rockPaperScrissor(firstPlayer: .rock, secondPlayer: .rock)
rockPaperScrissor(firstPlayer: .rock, secondPlayer: .paper)
rockPaperScrissor(firstPlayer: .rock, secondPlayer: .scissor)
rockPaperScrissor(firstPlayer: .scissor, secondPlayer: .rock)
rockPaperScrissor(firstPlayer: .scissor, secondPlayer: .scissor)
rockPaperScrissor(firstPlayer: .scissor, secondPlayer: .paper)
