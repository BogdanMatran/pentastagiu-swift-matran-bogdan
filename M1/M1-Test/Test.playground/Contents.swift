//: Playground - noun: a place where people can play

import Foundation
enum MenuType {
    case vegan
    case vegetarian
    case normal
    var description: String {
        switch self {
        case .vegan:
            return "vegan"
        case .normal:
            return "normal"
        case .vegetarian:
            return "vedetarian"
        }
    }
}

enum CarboDrinks {
    case Cola
    case Fanta
    case Sprite
    case SparklingWater
    var description: String {
        switch self {
        case .Cola:
            return "Cola"
        case .Fanta:
            return "Fanta"
        case .SparklingWater:
            return "SparklingWater"
        case .Sprite:
            return "Sprite"
        }
    }
}

enum HotDrinks {
    case coffee
    case hotCiocolate
    case tea
}

enum Dishes {
    case soup
    case grilledVegetables
    case chicken
    case pork
    case salad
    case desert
}

struct MenuDate {
    var day: Int
    var month: Int
    var year: Int
    //C6
    mutating func setDateToCurrentDate(newDay: Int, newMonth: Int, newYear: Int) {
        day = newDay
        month = newMonth
        year = newYear
    }
}

class Menu {
    var menuName : String
    var menuType: MenuType
    var carboDrinks: Set<CarboDrinks>
    var hotDrinks: Set<HotDrinks>
    var dishes: Set<Dishes>
    var isTodaysMenu: Bool
    var chefsDish: String?
    var menuDate: MenuDate
    var price: Double
    
    init(menuName : String,
         menuType: MenuType,
         carboDrinks: Set<CarboDrinks>,
         hotDrinks: Set<HotDrinks>,
         dishes: Set<Dishes>,
         isTodaysMenu: Bool,
         chefsDish: String?,
         menuDate: MenuDate,
         price: Double) {
        self.menuName  = menuName
        self.menuType = menuType
        self.carboDrinks = carboDrinks
        self.hotDrinks = hotDrinks
        self.dishes = dishes
        self.isTodaysMenu = isTodaysMenu
        self.chefsDish = chefsDish
        self.menuDate = menuDate
        self.price = price
    }
    
    //C1
    func showMenuDetails() {
        var menuDetails: String = "/c1 Meniul "
        if isTodaysMenu {
            menuDetails = menuDetails + "\(menuName ) de tipul \(menuType) , in data de \(menuDate) contine \(carboDrinks) , \(hotDrinks) si face parte din meniul zilei . Specialitatea bucatarului de astazi este \(chefsDish!). Pretul este \(price) lei."
        } else {
            menuDetails = menuDetails + "\(menuName ) de tipul \(menuType) , in data de \(menuDate) contine \(carboDrinks) , \(hotDrinks) si nu face parte din meniul zilei . Specialitatea bucatarului de astazi este \(chefsDish!). Pretul este \(price) lei."
        }
        print(menuDetails)
    }
    
    //C2
    func showMenuDetailsByChefsDish(){
        var menuDetails: String = "/c2 Meniul "
        if chefsDish != nil {
            menuDetails = menuDetails + "\(menuName ) de tipul \(menuType) , in data de \(menuDate) contine \(carboDrinks) , \(hotDrinks) . Specialitatea bucatarului de astazi este \(chefsDish!). Pretul este \(price) lei."
        }
        print(menuDetails)
    }
    
    // C3
    func showMenuName() {
        let menuName = self.menuName.uppercased()
        print(menuName)
    }
    func showMenuType() {
        print(menuType.description.lowercased())
    }
}

let menuDate = MenuDate(day: 13, month: 5, year: 2018)
let carboDrinks: [CarboDrinks] = [.Cola, .SparklingWater, .Fanta, .Sprite]
let carboDrinksSet: Set<CarboDrinks> = Set(carboDrinks)
let hotDrinks: [HotDrinks] = [.coffee, .tea, .hotCiocolate]
let hotDrinksSet: Set<HotDrinks> = Set(hotDrinks)
let dishes: [Dishes] = [.soup, .desert, .grilledVegetables, .chicken , .grilledVegetables, .pork , .salad ]
let dishesSet: Set<Dishes> = Set(dishes)
let menuBistro = Menu(menuName : "Meniu frumos",
                      menuType: MenuType.normal,
                      carboDrinks: carboDrinksSet,
                      hotDrinks: hotDrinksSet,
                      dishes: dishesSet,
                      isTodaysMenu: true,
                      chefsDish: "Coaste la gratar",
                      menuDate: menuDate,
                      price: 20.6)
menuBistro.showMenuDetails()
menuBistro.showMenuDetailsByChefsDish()
menuBistro.showMenuName()
menuBistro.showMenuType()

//C4
menuBistro.dishes.insert(.soup)
menuBistro.dishes.insert(.salad)
menuBistro.dishes.insert(.chicken)
if menuBistro.carboDrinks.count < 3 {
    print("Trebuie sa fie minim 3 bauturi carbogazoase")
}

//C5
var sortedDrinks: Set<CarboDrinks> = menuBistro.carboDrinks
sortedDrinks.sorted { (carboDrinkOne, carboDrinkTwo) -> Bool in
    return carboDrinkOne.description < carboDrinkTwo.description
}

//C6
if menuBistro.menuDate.day != 14 || menuBistro.menuDate.month != 5 || menuBistro.menuDate.year != 2018 {
    menuBistro.menuDate.setDateToCurrentDate(newDay: 14, newMonth: 5, newYear: 2018)
}

//C7
func showMenusDetails(menus: [Menu]) {
    for menu in menus {
        print(menu.showMenuDetails())
    }
}
let menuBistroVegan = Menu(menuName : "Meniu vegan",
                           menuType: MenuType.vegan,
                           carboDrinks: carboDrinksSet,
                           hotDrinks: hotDrinksSet,
                           dishes: dishesSet,
                           isTodaysMenu: true,
                           chefsDish: "Coaste la gratar",
                           menuDate: menuDate,
                           price: 2.70)
let menuBistroNormal = Menu(menuName : "Meniu normal",
                            menuType: MenuType.normal,
                            carboDrinks: carboDrinksSet,
                            hotDrinks: hotDrinksSet,
                            dishes: dishesSet,
                            isTodaysMenu: false,
                            chefsDish: "Varza la gratar",
                            menuDate: menuDate,
                            price: 10.10)
let menuBistroVegetarian = Menu(menuName : "Meniu vegetarian",
                                menuType: MenuType.vegetarian,
                                carboDrinks: carboDrinksSet,
                                hotDrinks: hotDrinksSet,
                                dishes: dishesSet,
                                isTodaysMenu: true,
                                chefsDish: "Pulpe la gratar",
                                menuDate: menuDate,
                                price: 20.29)
showMenusDetails(menus: [menuBistroVegan, menuBistroNormal, menuBistroVegetarian])

//C8
func compareMenusPrices(menus: [Menu], price: Double) -> Menu? {
    for menu in menus {
        if menu.price > price - 5 && menu.price < price + 5 {
            return menu
        }
    }
    return nil
}
let cheapMenu = compareMenusPrices(menus: [menuBistroVegetarian, menuBistroNormal, menuBistroVegan], price: 15)

//C9
func countTodaysMenu(menus: [Menu]) {
    var count = 0
    for menu in menus {
        if menu.isTodaysMenu {
            count = count + 1
        }
    }
    print("\(count) din \(menus.count) sunt meniul zilei")
}
countTodaysMenu(menus: [menuBistroVegetarian, menuBistroNormal, menuBistroVegan])

/* Rezolvarea ta a fost foarte buna per general! Vreau sa te felicit pentru progresul tau si sa iti urez success pe mai departe!
 Tine-o tot asa! :) 
 */