//: Playground - noun: a place where people can play

import Foundation

//P3
func getDays(digit: UInt){
    switch digit {
    case 1:
        print("Monday")
    case 2:
        print("Tuesday")
    case 3:
        print("Wednesday")
    case 4:
        print("Thurday")
    case 5:
        print("Friday")
    case 6:
        print("Saturday")
    case 7:
        print("Sunday")
    default:
        print("Not a valid day")
    }
}
getDays(digit: 4)
getDays(digit: 9)

//P6
func friend(number: UInt) {
    print("Prietenii lui \(number) sunt ")
    for index in 1 ..< number {
        if number/index == number % index {
            print("\(index) , ")
        }
    }
}
friend(number: 18)
//P8
enum Op{
    case adunare
    case scadere
    case inmultire
    case impartire
}
func operatie(a: Int, b: Int, operatie: Op) {
    switch operatie {
    case .adunare:
        print("\(a) + \(b) = \(a + b)")
    case .scadere:
        print("\(a) - \(b) = \(a - b)")
    case .inmultire:
        print("\(a) * \(b) = \(a * b)")
    case .impartire:
        print("\(a) % \(b) = \(a % b)")
    }
}
operatie(a: 2, b: 4, operatie: .adunare)
operatie(a: 2, b: 4, operatie: .scadere)
operatie(a: 2, b: 4, operatie: .inmultire)
operatie(a: 10, b: 4, operatie: .impartire)
//P10
func palindrom(numar: inout UInt) {
    let copyOfNumber: UInt = numar
    var reversOfNumber: UInt = 0
    while (numar != 0){
        reversOfNumber = reversOfNumber * 10 + numar % 10
        numar = numar / 10
    }
    if reversOfNumber == copyOfNumber {
        numar = reversOfNumber
        print("\(numar)")
    }
    else {
        numar = 0
        print("\(numar)")
    }
}
var a: UInt = 121
palindrom(numar: &a)
//P4

func divizori(k: Int, n: Int) {
    var number: Int = n
    while number != 0 {
        for index in 1 ... 100 {
            if index / k == 0 {
                number = number - 1
                print("\(index) , ")
            }
        }
    }
}
divizori(k: 4, n: 2)

