//: Playground - noun: a place where people can play

import UIKit

func getDigitsAndDecimals(for number: Int) -> ([Int], Int) {
    var digitsArray: [Int] = [Int]()
    var decimals = 1
    var number = number
    while (number != 0) {
        digitsArray.append(number % 10)
        shortByTen(number:&number)
        decimals *= 10
    }
    return (digitsArray, decimals)
}

func shortByTen(number: inout Int) {
    number /= 10
}

func doubleFirstDigit(of number: inout Int) {
    var finalNumber = 0
    var firstDigit = 0
    var digitsArray: [Int] = getDigitsAndDecimals(for: number).0
    var decimals =  getDigitsAndDecimals(for: number).1
    digitsArray = digitsArray.reversed()
    firstDigit = digitsArray[0] * 2
    finalNumber = firstDigit
    shortByTen(number:&decimals)
    digitsArray.remove(at: 0)
    var stringNumber = String(firstDigit)
    for aDigit in digitsArray {
        stringNumber.append(String(aDigit))
    }
    finalNumber = Int(stringNumber)!
    print(finalNumber)
}

var num: Int = 800
doubleFirstDigit(of: &num)
