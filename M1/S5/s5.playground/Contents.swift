//: Playground - noun: a place where people can play

import Foundation
enum Marca {
    case Kia
    case BMW
    case VW
    case Audi
    case Telsa
    case Dacia
}
struct Confort {
    var incalzireScaune: Bool
    var navigatie: Bool
    var nrScaune: UInt
    func getConfort() -> String {
        var confortString = ""
        if incalzireScaune && navigatie{
        confortString = "Are incalzire in scaune si navigatie"
        } else if incalzireScaune == false && navigatie == false {
        confortString = "Nu are incalzire si nici navigatie"
        } else if incalzireScaune == true && navigatie == false {
          confortString = "Are incalzire dar nu are navigatie"
        } else if incalzireScaune == false && navigatie == true {
            confortString = "Nu are incalzire dar are navigatie"
        }
        return confortString
    }
}
class Masina {
    var anFabricatie: UInt
    var nrInmatriculare: String
    var marca: Marca
    var culoare: String
    var Itp: String
    var nrKm: UInt
    var senzoriParcare: Bool
    var confort: Confort
    init(anFabricatie: UInt, nrInmatriculare: String, marca: Marca, culoare: String, Itp: String, nrKm: UInt, senzoriParcare: Bool, confort: Confort) {
        self.anFabricatie = anFabricatie
        self.nrInmatriculare = nrInmatriculare
        self.marca = marca
        self.culoare = culoare
        self.Itp = Itp
        self.nrKm = nrKm
        self.senzoriParcare = senzoriParcare
        self.confort = confort
    }
    func esteMasinaDeFamilie() -> Bool {
        var familie = false
        if (confort.nrScaune > 2 && confort.nrScaune <= 8) && (marca == .Kia || marca == .VW || marca == .Audi){
          familie = true
        }
        return familie
    }
    func getDescriere() {
       
        if senzoriParcare {
        print("Masina \(marca) din anul \(anFabricatie) are numar de inmatriculare \(nrInmatriculare) cu numarul de km = \(nrKm) cu senzori de parcare \( confort.getConfort())")
        } else {
        print("Masina \(marca) din anul \(anFabricatie) are numar de inmatriculare \(nrInmatriculare) cu numarul de km = \(nrKm) fara senzori de parcare \( confort.getConfort())")
        }
    }
    func resetKm() -> UInt {
        nrKm = 0
        return nrKm
    }
    func getKmAn() {
        let ani = 2018 - anFabricatie
        print("Km pe an = \(nrKm/ani)")
    }
}
var myConfort = Confort(incalzireScaune: true, navigatie: true, nrScaune: 6)
var masinaMea = Masina(anFabricatie: 1993, nrInmatriculare: "IS 20 BOG", marca: .Audi, culoare: "Rosie", Itp: "20 mai 2018", nrKm: 20000, senzoriParcare: true, confort: myConfort)
masinaMea.getDescriere()
if (masinaMea.esteMasinaDeFamilie()){
    print("Este masina de familie")
}

print(masinaMea.nrKm)
masinaMea.getKmAn()
masinaMea.resetKm()
