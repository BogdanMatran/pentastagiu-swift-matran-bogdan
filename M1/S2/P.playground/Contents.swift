//: Playground - noun: a place where people can play

import Foundation
//Tema Pentastagiu
// P1
func computeCircleArea(radius: Double?) {
    if radius != nil {
    let area = Double.pi * (radius! * radius!)
        print("Aria cercului \(area)")}
    else{
          print("raza este nil")
        }
}
computeCircleArea(radius: 4)
//P2
func computeSum(number: Int?) {
    var sum = 0
    if var n = number {
    while(n != 0) {
        sum = sum + (n % 10)
        n = n / 10
    }
    print("Suma cifrelor este \(sum)")
    } else {
        print("Erroare ! numarul este nil")
    }
}
computeSum(number: 123)
//P3
func triangleType(a: UInt, b: UInt, c: UInt) {
    if (a == 0 || b == 0 || c == 0) {
        print("Latura unui triunghi nu poate fi 0 ")
    }
    if (a+b < c || a+c < b || b+c < a) {
        print("Nu este un triunghi")
        return
    } else if (a == b) && (b == c) {
        print("Triungi echilateral")
        return
    } else if (a == b) || (b == c) || (a == c) {
        print("Triunghi Isoscel")
        return
    } else if((a*a == b*b + c*c) || (b*b == a*a + c*c) || (c*c == a*a + b*b)) {
        print("Triunghi dreptunghic")
        return
    }
    else {
        print("Triunghi oarecare")
    }
}
triangleType(a: 0, b: 2, c: 1)
triangleType(a: 9, b: 2, c: 3)
triangleType(a: 2, b: 2, c: 2)
triangleType(a: 3, b: 3, c: 1)
triangleType(a: 4, b: 3, c: 5)
triangleType(a: 10, b: 12, c: 4)
//P4
func sumOfNumbersUntilN(number: Int) {
    var sum = 0
    for i in 1 ..< number{
        sum = sum + i
    }
    print("Suma numerelor pana la \(number) este \(sum)")
}
sumOfNumbersUntilN(number: 4)
//P5
func factorial(number: Int) {
    var fact = 1
    var i = 2
    while (i <= number) {
        fact = fact * i
        i = i + 1
    }
    print("Factorialul numarului \(number) este \(fact)")
}
factorial(number: 3)
//P6
func sumaFormula (numar: Int) {
    let sum = numar*((numar + 1)/2)
    print("Suma dupa formula pana la \(numar) este \(sum) ")
}
sumaFormula(numar: 4)
//P9
func numarPrim(number: Int) -> Int {
    var nrDivizori = 0
    var status = 0
    for i in 1 ... number {
        if number%i == 0 {
            nrDivizori = nrDivizori + 1
        }
    }
    if nrDivizori > 2 {
        status = 0
        print("Numarul \(number) nu este prim")
    } else if nrDivizori == 2 {
        status = 1
        print("Numarul \(number) este prim")
    }
    return status
}
numarPrim(number: 9)
//P7
func primeleNumerePrime(number: Int) {
    var listOfNumbers = [Int]()
    var listOfPrimenumbers = [Int]()
    listOfPrimenumbers.append(1)
    for i in 1 ..< number+1 {
        listOfNumbers.append(i)
    }
    
    for item in listOfNumbers {
        //am folosit functia care determina daca un numar este prim
        if numarPrim(number: item) == 1 {
            listOfPrimenumbers.append(item)
        }
    }
    print("Numerele prime pana la \(number) sunt \(listOfPrimenumbers)")
}
primeleNumerePrime(number: 9)

//P8
func maxDigit(number: Int) {
    var n = number
    var max = n % 10
    while (n != 0) {
        n = n / 10
        if (max < n % 10) {
            max = n % 10
        }
    }
    print("Maximul dintre cifre este \(max)")
}
maxDigit(number: 234)

//P10
func cmmdc(a: Int, b: Int) {
    var second = b
    var first = a
    while (first != second)
    {
        if(first > second) {
            first = first - second
        } else {
            second = second - first
        }
    }
    print("Cel mai mic divizor comun este \(first)")
}
cmmdc(a: 6, b: 9)

