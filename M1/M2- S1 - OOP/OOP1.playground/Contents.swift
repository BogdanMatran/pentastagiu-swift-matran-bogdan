//: Playground - noun: a place where people can play

import Foundation

enum Type {
    case carnivores
    case herbivores
}

enum WaterType {
    case salty
    case fresh
}

protocol Animal {
    var lives: UInt { get set }
    var name: String { get set}
    var gender: String { get set }
    var age: Float { get set }
    func makeNoise()
}

class Fish: Animal {
    var lives: UInt
    var name: String
    var gender: String
    var age: Float
    var waterType: WaterType
    func makeNoise() {
        print("Blup Blup")
    }
    init(lives: UInt, name: String, gender: String, age: Float, waterType: WaterType) {
        self.lives = lives
        self.age = age
        self.gender = gender
        self.name = name
        self.waterType = waterType
    }
}

class Dolphin: Fish {
    var isBlue: Bool
    
    override func makeNoise() {
        print("I am a dolphin")
    }
    
    init(isBlue: Bool, lives: UInt, name: String, gender: String, age: Float, waterType: WaterType) {
        self.isBlue = isBlue
        super.init(lives: lives, name: name, gender: gender, age: age, waterType: waterType)
    }
}

class Trench: Fish {
    var dimensions: (UInt, UInt)
    
    override func makeNoise() {
        print("I am a fresh water fish!!")
    }
    
    init(dimensions: (UInt, UInt), lives: UInt, name: String, gender: String, age: Float, waterType: WaterType) {
        self.dimensions = dimensions
        super.init(lives: lives, name: name, gender: gender, age: age, waterType: waterType)
    }
}

let dolphin = Dolphin(isBlue: true, lives: 1, name: "Jacobo", gender: "M", age: 34, waterType: .salty)
dolphin.makeNoise()
let trench = Trench(dimensions: (3, 5), lives: 3, name: "Fisheeee", gender: "M", age: 2, waterType: .fresh)
trench.makeNoise()

class Mammal: Animal {
    var lives: UInt
    var name: String
    var gender: String
    var limbs: UInt
    var age: Float
    var numberOfBirths: UInt
    var species: Type
    func makeNoise(){
        print("Mammal")
    }
    
    init(lives: UInt, limbs: UInt, name: String, gender: String, age: Float, numberOfBirths: UInt, species: Type) {
        self.age = age
        self.lives = lives
        self.gender = gender
        self.name = name
        self.species = species
        self.limbs = limbs
        self.numberOfBirths = numberOfBirths
    }
}

class Dog: Mammal {
    var color: String
    
    init(lives: UInt, limbs: UInt, color: String, age: Float, name: String, gender: String,numberOfBirths: UInt, species: Type) {
        self.color = color
        super.init(lives: lives, limbs: limbs, name: name, gender: gender, age: age, numberOfBirths: numberOfBirths, species: species)
    }
    
    override func makeNoise(){
        print("Ham Ham")
    }
}

class Cat: Mammal {
    var breed: String
    
  override func makeNoise() {
        print("Meow")
    }
    
    init(breed: String, lives: UInt, limbs: UInt, name: String, gender: String, age: Float, numberOfBirths: UInt, species: Type) {
        self.breed = breed
        super.init(lives: lives, limbs: limbs, name: name, gender: gender, age: age, numberOfBirths: numberOfBirths, species: species)
    }
}

class Hamster: Mammal {
    var cageSize: UInt
    
    init(cageSize: UInt, lives: UInt, limbs: UInt, name: String, gender: String, age: Float, numberOfBirths: UInt, species: Type) {
        self.cageSize = cageSize
        super.init(lives: lives, limbs: limbs, name: name, gender: gender, age: age, numberOfBirths: numberOfBirths, species: species)
    }
}

let cat = Cat(breed: "Egiptean", lives: 9, limbs: 4, name: "Pufina", gender: "F", age: 4, numberOfBirths: 2, species: .carnivores)
cat.makeNoise()
let hamster = Hamster(cageSize: 22, lives: 1, limbs: 4, name: "Soricica", gender: "F", age: 4, numberOfBirths: 0, species: .herbivores)
hamster.makeNoise()

class Bird: Animal {
    var lives: UInt
    var limbs: UInt
    var color: String
    var age: Float
    var name: String
    var gender: String
    var canFly: Bool
    
    func makeNoise(){
        print("Qok")
    }
    
    init(lives: UInt, limbs: UInt, color: String, age: Float, name: String, gender: String, canFly: Bool) {
        self.limbs = limbs
        self.lives = lives
        self.color = color
        self.age = age
        self.name = name
        self.gender = gender
        self.canFly = canFly
    }
}

class Parrot: Bird {
    var cageSize: UInt
    
    override func makeNoise() {
       // super.makeNoise()
        print("Prrrra")
    }
    
    init(cageSize: UInt,lives: UInt, limbs: UInt,  color: String, age: Float, name: String, gender: String, canFly: Bool) {
        self.cageSize = cageSize
        super.init(lives: lives, limbs: limbs, color: color, age: age, name: name, gender: gender, canFly: canFly)
    }
}

class Chicken: Bird {
    var eggsADay: UInt
    
    override func makeNoise() {
        print("Cot co dac")
    }
    
    init(eggsADay: UInt,lives: UInt, limbs: UInt, color: String, age: Float, name: String, gender: String, canFly: Bool) {
        self.eggsADay = eggsADay
        super.init(lives: lives, limbs: limbs, color: color, age: age, name: name, gender: gender, canFly: canFly)
        
    }
}

let bird = Bird(lives: 2, limbs: 5, color: "red", age: 12, name: "ION", gender: "M", canFly: true)
let parrot = Parrot(cageSize: 4,lives: 3, limbs: 2, color: "Blue", age: 34, name: "COCO", gender: "F", canFly: true)
let chicken = Chicken(eggsADay: 2,lives: 5, limbs: 3, color: "white", age: 123, name: "Jana", gender: "F", canFly: false)

bird.makeNoise()
parrot.makeNoise()
chicken.makeNoise()
print(parrot.cageSize)

let dog = Dog(lives: 2, limbs: 4, color: "Dark", age: 5, name: "Labus", gender: "M", numberOfBirths: 5, species: .carnivores)

func printNoise(from bird: Bird) {
    bird.makeNoise()
}

func printNoise(from dog: Dog,and bird: Bird) {
    dog.makeNoise()
    bird.makeNoise()
}

printNoise(from: bird)
printNoise(from: parrot)
printNoise(from: chicken)
printNoise(from: dog, and: bird)

//if parrot is Bird {
//    print("\(parrot.name) is bird")
//} else {
//    print("\(parrot.name) is not a bird")
//}
//
//if dog is Bird {
//    print("\(dog.name) is bird")
//} else {
//    print("\(dog.name) is not a bird")
//}
//
//if bird is Parrot {
//    print("\(bird.name) is a parrot")
//} else {
//    print("\(bird.name) is not a parrot")
//}
//
//if let newBird = bird as? Parrot {
//    print("Parrot as Bird")
//}
//let newBird2 = parrot as! Bird

